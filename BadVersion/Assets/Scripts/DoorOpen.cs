using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour
{
    public GameObject wall;
    public Animator animator;
    public GameObject button;
    public GameObject buttonused;
    public GameObject cube;
    public GameObject camera;
    public GameObject camera2;
    public AudioSource doorsound;

    void Start()
    {
        
    }


    void Update()
    {
        
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == cube)
        {
            doorsound.Play();
            animator.Play("dooropen");
            camera.SetActive(false);
            camera2.SetActive(true);
            button.SetActive(false);
            buttonused.SetActive(true);
        }
    }

}
