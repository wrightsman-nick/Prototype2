using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public GameObject cube;

    public float moveSpeed;
    public float shrinkDelta = 0.5f;

    private int shrinks;
    private int grows;

    private float nextShrink = 1f;
    private float myTime = 0.0f;

    bool m_isPlayerShrunk;
    bool m_isPlayerGrown;
    bool m_isPlayerNorm;

    public AudioSource growAudio;
    public AudioSource shrinkAudio;

    private Vector3 scaleChange;

    void Start ()
    {
        m_isPlayerNorm = true;
        shrinks = 1;
        grows = 1;
        moveSpeed = 4f;
        scaleChange = new Vector3(0.7f, 0.7f, 0.7f);
    }

    void Update ()
    {
        myTime = myTime + Time.deltaTime;

        transform.Translate(moveSpeed*Input.GetAxis("Horizontal")*Time.deltaTime,0f,moveSpeed*Input.GetAxis("Vertical")*Time.deltaTime);

       if (Input.GetButton("Fire1") && myTime > nextShrink && m_isPlayerNorm)
        {
            shrinkAudio.Play();
            shrinks = shrinks - 1;
            nextShrink = myTime + shrinkDelta;
            cube.transform.localScale -= scaleChange;
            nextShrink = nextShrink - myTime;
            myTime = 0.0f;
            m_isPlayerShrunk = true;
        }

        if (Input.GetButton ("Fire3") && myTime > nextShrink && m_isPlayerNorm)
        {
            growAudio.Play();
            grows = grows - 1;
            nextShrink = myTime + shrinkDelta;
            cube.transform.localScale += scaleChange;
            nextShrink = nextShrink - myTime;
            myTime = 0.0f;
            m_isPlayerGrown = true;
        }

        if (shrinks == 1 && grows == 1)
        {
            m_isPlayerNorm = true;
            m_isPlayerGrown = false;
            m_isPlayerShrunk = false;
        }

        if (m_isPlayerNorm)
        {
            grows = 1;
            shrinks = 1;
        }

        if (Input.GetButton ("Cancel"))
        {
            Application.Quit();
        }
    }
}