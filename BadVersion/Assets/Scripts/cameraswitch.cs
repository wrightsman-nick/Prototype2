using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraswitch : MonoBehaviour
{
    public GameObject cube;
    public GameObject camera2;
    public GameObject camera3;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == cube)
        {
            camera2.SetActive(false);
            camera3.SetActive(true);
        }
    }
}
