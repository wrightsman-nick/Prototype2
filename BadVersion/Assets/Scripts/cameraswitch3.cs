using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraswitch3 : MonoBehaviour
{
    public GameObject cube;
    public GameObject camera4;
    public GameObject camera5;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == cube)
        {
            camera4.SetActive(false);
            camera5.SetActive(true);
        }
    }
}
